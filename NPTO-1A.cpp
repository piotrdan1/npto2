﻿#include <iostream>

using namespace std;

int main() 
{
    int liczba;

    cout<<"Podaj liczbe: ";
    cin>>liczba;

    for (int i = 1; i <= liczba; i++)
    {
        if (i%2==0)
        {
            cout << i << "-> PARZYSTA" << endl;
        }
        else
        {
            cout << i << "-> NIEPARZYSTA" << endl;
        }
    }
    
    return 0;
}